from tkinter import *
from tkinter import filedialog
import threading
import socket
import time
import re
import sys
import os
from configparser import ConfigParser


class Application:

    def __init__(self): 
        
        # Initialisation de la fenetre
        self.Window = Tk() 
        self.Window.withdraw() 
          

        self.userConnect = []
        self.UserChatPrivee = []
        self.status = True

        # login window 
        self.login = Toplevel() 

        self.login.title("Connexion")  
        self.login.geometry("450x250")

        self.pls = Label(self.login,  
                       text = "Entrer votre pseudo", 
                       justify = CENTER,  
                       font = "14") 
          
        self.pls.pack() 

 
        self.labelName = Label(self.login, 
                               text = "Pseudo: ", 
                               font = "12") 
        self.labelName.pack() 
          
        #Saisie message
        self.entryName = Entry(self.login,  
                             font = "14") 
        self.entryName.pack() 
        self.entryName.focus() 
          
        # Bouton pour soumettre sa demande de connexion
        self.go = Button(self.login, 
                         text = "Se connecter",  
                         font = "14",  
                         command = self.start) 
        self.info = Label(self.login,  
                text = "Charactères autorisés : lettres, nombres, _ (max 15)", 
                justify = CENTER,  
                font = "14")
        self.info.pack()
          
        self.go.pack() 
        self.Window.mainloop() 


    def start(self):
        """
            Connexion de l'utilisateur
        """
        self.name = self.entryName.get().upper()

        snd= threading.Thread(target = self.sendMessage(self.name, 'CONNECT ')) 
        snd.start() 
        snd.join()

        #On écoute ici la reponse de la commande CONNECT
        message= sock.recv(256).decode()
        msgConnect = message.split()
        
        #Verifie que le pseudo est valide
        if msgConnect[0] == "110":
            self.login.destroy()
            self.salon(self.name)
            ec = threading.Thread(target=self.ecoute)
            listUser = threading.Thread(target=self.listConnected)
            ec.start()
            listUser.start()
        elif msgConnect[0] == "310":
            self.info['text'] = "Entrer un pseudo valide"
        elif msgConnect[0] == "340":
            self.info['text'] = "Ce pseudo existe déjà"


            
    def quit(self): 
        """
            Fonction pour quitter le chat
        """
        snd = threading.Thread(target=self.sendMessage(self.name, 'QUIT '))
        snd.start()
        snd.join()


    def salon(self, name):
        """
            Fenetre principale une fois connecté
        """

        self.Window.title("Salon")
        self.Window.deiconify() 

        #Configuration de la fenetre
        self.Window.configure(width = 650, 
                              height = 600, 
                              bg = "grey") 
        self.Window.resizable(width = False, 
                              height = False) 
        
        #Menu de la fenetre
        self.menuBar = Menu(self.Window)
        self.Window.config(menu=self.menuBar)
        self.menuFile = Menu(self.menuBar, tearoff=0)
        self.menuBar.add_cascade(label="Option", menu=self.menuFile)
        self.menuFile.add_command(label="Rename", command=self.fenetreRename)
        self.menuFile.add_command(label="Quitter", command=self.quit)


        #Banniere contenant le pseudo de l'utilisateur
        self.labelHead = Label(self.Window, 
                             bg = "black",  
                              fg = "white", 
                              text = self.name, 
                               font = "13", 
                               padx = 5,
                               pady = 5) 

        self.labelHead.place(relwidth = 1)

          
        # C'est ici que le texte s'affiche
        self.textCons = Text(self.Window, 
                             bg = "#17202A", 
                             fg = "#EAECEE", 
                             font = "14",  
                             pady = 5) 
          
        self.textCons.place(relheight = 0.745,
                            relwidth = 0.5,rely = 0.08) 

        #Liste des utilisateurs connectés
        self.UserListe = Label(self.Window, 
                                width=10,
                                height = 2, 
                                bg = "#17202A", 
                                fg = "#EAECEE", 
                                font = "14",  
                                padx = 5, 
                                pady = 5) 
          
        self.UserListe.place(relheight = 0.745,
                            relwidth = 0.5,
                            x=350,
                            rely = 0.08) 
        
        self.listBox = Listbox(self.UserListe,
                  height = 10,  
                  width = 15,  
                  bg = "grey", 
                  activestyle = 'dotbox',  
                  font = "Helvetica", 
                  fg = "yellow")

        self.listBox.pack()
        

        # Boutton afk et quitter
        self.ButtonAFK = Button(self.UserListe, text='Passer AFK', command=self.changer_status)
        self.ButtonAFK.pack()
        
        self.ButtonQuitter = Button(self.UserListe, text='Quitter', command=self.quit)
        self.ButtonQuitter.pack(side=BOTTOM)

        # LabelBottom est la zone contenant le champ de saisie et le button send  
        self.labelBottom = Label(self.Window, 
                                 bg = "#ABB2B9", 
                                 height = 80) 

        self.labelBottom.place(relwidth = 0.5, 
                               rely = 0.825) 
          
        self.entryMsg = Entry(self.labelBottom, 
                              bg = "#2C3E50", 
                              fg = "#EAECEE", 
                              font = "13") 

        self.entryMsg.place(relwidth = 1, 
                            relheight = 0.06, 
                            rely = 0.008, 
                            relx = 0.011) 
        #Focus sur le champ de saisie des message a l'ouverture de la fenetre
        self.entryMsg.focus() 
          
        # create a Send Button 
        self.buttonMsg = Button(self.labelBottom, 
                                text = "SEND", 
                                font = "10",  
                                width = 20, 
                                bg = "#ABB2B9", 
                                command = lambda : self.sendButton(self.entryMsg.get(), 'ALL ', self.entryMsg))
          
        self.buttonMsg.place(relx = 0.77, 
                             rely = 0.008, 
                             relheight = 0.06,  
                             relwidth = 0.22) 
          
        self.textCons.config(cursor = "arrow") 
          
        # Scroll bar 
        scrollbar = Scrollbar(self.textCons) 

          
        scrollbar.config(command = self.textCons.yview) 
          
        self.textCons.config(state = DISABLED) 
  
    def changer_status(self):
        """
            Fonction utlisé pour changer de status
        """
        if self.status:
            self.ButtonAFK['text'] = 'Passer en ligne'
            snd= threading.Thread(target = self.sendMessage(" ", 'AFK ')) 
            self.status = False
        else:
            self.ButtonAFK['text'] = 'Passer AFK'
            snd= threading.Thread(target = self.sendMessage(" ", 'ACTIVE ')) 
            self.status = True

        snd.start() 
        snd.join()

        
    def sendButton(self, msg, command, champSaisie): 
        """
            fonction du bouton SEND (all et whisper)
            Lance un thread pour envoyer un message
        """
        champSaisie.delete(0, END) 
        snd= threading.Thread(target = self.sendMessage(msg, command)) 
        snd.start()
        snd.join()



    def fenetreRename(self):
        """
            Creation de la fenetre pour RENAME
        """
        self.popRename = Toplevel()
        self.popRename.title("Rename")

        lab = Label(self.popRename, 
                    text = "Entrez un nouveau pseudo", 
                    justify = CENTER,  
                    font = "14")

        self.entree = Entry(self.popRename)

        self.infoRename = Label(self.popRename, 
                                text = "Charactère autorisés : lettre, nombre, _ (max 15)", 
                                justify = CENTER,  
                                font = "14")           
        
        buttonRename = Button(self.popRename, text="Valider", command = self.Rename)

        self.entree.pack()
        self.infoRename.pack()
        lab.pack()
        buttonRename.pack()

    def Rename(self):
        """
            Recupere le nom entré
            lance un thread pour envoyer la requete
        """
        self.name = self.entree.get().upper()
        self.labelHead['text'] = self.name
        snd= threading.Thread(target = self.sendMessage(self.name, 'RENAME ')) 
        snd.start() 
        snd.join()

        self.entree.delete(0, END) 


    def ecoute(self):
        """
            Ecoute toutes les requetes provenant du server
        """

        msg = f"Bienvenue à toi {self.name}"            
        self.afficherMessage(msg)
        request_from_user = []
        request_to_user = [] 

        while True:
            message = sock.recv(256).decode()
            
            if message == '': break

            msgSplit = message.split(' ', 1)

            #Connexion d'un utilisateur (autre que nous)
            if msgSplit[0] == '110':
                msg = f"Bienvenue à toi {msgSplit[1]}"            
                self.afficherMessage(msg) 
            
            #Fermer le chat
            if msgSplit[0] == '190':
                self.afficherMessage('~Systeme~ : Au revoir !')
                time.sleep(3)
                sock.close()
                print('fin')
                break
            
            #Indiquer que quelqu'un quitte le chat
            if msgSplit[0] == '195':
                msgSplit2 = message.split()
                pseudo = msgSplit2[1]
                self.afficherMessage(f'~Systeme~ : {pseudo} s\'est déconnecté !')

            #messages a tout le monde
            elif msgSplit[0] == '105':
                msgSplit2 = message.split(' ', 2)
                pseudo = msgSplit2[1]
                text = msgSplit2[2]
                m = f'[{pseudo}] : {text}'
                self.afficherMessage(m)

            #Afficher la liste des clients
            elif msgSplit[0] == '100':
                """
                    Affichage de la liste des utilisateurs
                """
                self.traitementUserList(msgSplit[1])

            #Connexion aux chats privés
            elif msgSplit[0] == '150':
                """
                    Reponse de CHAT
                """
                request_to_user.append(msgSplit[1])             
                if (msgSplit[1] in request_to_user) and (msgSplit[1] in request_from_user) :
                    self.fenetreChatPrivee(msgSplit[1])

            elif msgSplit[0] == '151':
                """
                    On ferme le chat
                """
                m = f"~Systeme~ Chat privée avec {msgSplit[1]} terminé..." 
                request_to_user.remove(msgSplit[1])
                request_from_user.remove(msgSplit[1])
                self.fenetreChat.destroy()
                self.afficherMessage(m)

            elif msgSplit[0] == '152':
                """
                    Quelqu'un veut chatter en privée avec vous
                """
                m = f'~Systeme~ {msgSplit[1]} veut chatter en privée avec vous...'
                request_from_user.append(msgSplit[1])
                if (msgSplit[1] in request_to_user) and (msgSplit[1] in request_from_user) :
                    self.fenetreChatPrivee(msgSplit[1])
                else :
                    self.afficherMessage(m)

            elif msgSplit[0] == '301':
                """
                    La personne n'est pas sur le serveur (inutile ?)
                """
                m = f"~Systeme~ : {msgSplit[1]} n'est pas sur le serveur"
                self.afficherMessage(m)

            elif msgSplit[0] == '173':
                """
                    Messages privées
                """

                msgSplit2 = message.split(' ', 2)
                pseudo = msgSplit2[1]
                text = msgSplit2[2]
                m = f'[{pseudo}] : {text}'
                self.TableauMessage.config(state = NORMAL) 
                self.TableauMessage.insert(END, m+"\n\n") 
                self.TableauMessage.config(state = DISABLED) 
                self.TableauMessage.see(END)

            #Gestion de Rename 
            elif msgSplit[0] == "141":

                snd= threading.Thread(target = self.sendMessage(' ', 'LISTCONNECTED ')) 
                snd.start()  
                snd.join()

            elif msgSplit[0] == '142':

                msgSplit2 = message.split()
                m = f'~System~ : {msgSplit2[1]} se nomme maintenant ->{msgSplit2[2]}'
                self.afficherMessage(m)

            #Code retour : SENDFILE, ACCEPTFILE, REFUSEFILE
            elif msgSplit[0] == "191" :
                """ 
                    Message bien reçu
                """
                pass

            elif msgSplit[0] == "192":
                """
                    Ouvre une popUP pour repondre a la proposition d'envoie d'un fichier
                """
                msgSplit2 = message.split()
                
                user_send_file = msgSplit2[2]
                name_file = msgSplit2[3]

                self.popUp_file(user_send_file, name_file)
            
            elif msgSplit[0] == "193":
                """
                    L'utilisateur a accepter qu'on lui envoie un fichier
                """

                msgSplit2 = message.split()
                user_receive_file = msgSplit2[1]
                name_file = msgSplit2[2]
                port = msgSplit2[3]
                adresse_ip_receveur = msgSplit2[4]
                
                m = f'~System~ : {user_receive_file} a accepté de recevoir le fichier : {name_file}'
                self.afficherMessage(m)
                
                # On envoie donc notre fichier à l'utilisateur
                s = threading.Thread(target=self.send_file(name_file, port, adresse_ip_receveur))
                s.start()

            elif msgSplit[0] == "194":
                """
                    L'utilisateur a accepter qu'on lui envoie un fichier
                """
                msgSplit2 = message.split()
                user_receive_file = msgSplit2[1]
                name_file = msgSplit2[2]
                
                self.afficherMessage(f"~System~ : {user_receive_file} a refusé {name_file} ")

            elif msgSplit[0] == "196":
                """
                    On a bien envoyé ACCEPTFILE 
                    On lance le thread pour recevoir le fichier
                """
                msgSplit2 = message.split()

                name_file = msgSplit2[1]
                port = msgSplit2[2]
                
                #On se met en attente du fichier
                self.afficherMessage(f'~Systeme~ : On attend de recevoir {name_file}...')
                thread_rec_file = threading.Thread(target=self.receive_file(port, name_file))
                thread_rec_file.start()

            # Verifie format pseudo pour le rename
            elif msgSplit[0] == "310":

                self.infoRename['text'] = "Entrer un pseudo valide"

            elif msgSplit[0] == "340":

                self.infoRename['text'] = "Ce pseudo existe déjà"

            #Changer de status
            elif msgSplit[0] == '120':

                if self.status:
                    m = f"~Systeme~ : Vous êtes Actif !"
                else :
                    m = f"~Systeme~ : Vous êtes AFK !"
                self.afficherMessage(m)
        
        self.Window.quit()



 

          
    def sendMessage(self, msg, command): 
        """
            Envoyer un message au serveur
        """

        while True:
            #Envoyer un message à tous le monde
            if re.search('^ALL ', command):
                message = (f"ALL {msg}")

            #envoyer une tentative de connexion
            elif re.search('^CONNECT ', command):
                message = (f"CONNECT {msg}")

            #Enoyer une tentative de rename
            elif re.search('^RENAME ', command):
                message = (f"RENAME {msg}")

            elif re.search('^LISTCONNECTED ', command):
                message = command
            
            # Changer de status
            elif re.search('^AFK ', command):
                message = f'AFK '
            elif re.search('^ACTIVE ', command):
                message = f'ACTIVE '

            # Chat privée
            elif re.search('^WHISPER ', command):
                message = f'{command} {msg}'
            
            elif re.search('^CHAT ', command):
                message = (f"CHAT {msg}")
            
            #Quitter le server
            elif re.search('^QUIT ', command):
                message = f'QUIT {self.name}'

            #Send file
            elif re.search('^SENDFILE ', command):
                message = (f"SENDFILE {msg}")
            
            elif re.search('^ACCEPTFILE ', command):
                message = f'ACCEPTFILE {msg}'

            elif re.search('^REFUSEFILE ', command):
                message = f'REFUSEFILE {msg}'
            else:
                break
                
            try:
                sock.sendall(message.encode())    
                break 
            except:
                break



    def receive_file(self, port, name_file):
        """
            Créer une socket et se met en attente pour recevoir le fichier 
        """
        try:
            socket_file = socket.socket()
            socket_file.bind(('', int(port)))
            socket_file.listen(2)
        except:
            print('Création socket pour écoute : échoué')


        while True:
            conn, adrr = socket_file.accept()
            f = open(name_file, 'wb')

            l = conn.recv(1024)
            while(l):
                self.afficherMessage('~Systeme~ : Receiving data...')
                f.write(l)
                l = conn.recv(1024)
            f.close()
            socket_file.close()
            self.afficherMessage(f'~Systeme~ : {name_file} a été téléchargé !')
            break
            

    def send_file(self, name_file, port, adresse_ip_receveur):
        """
            En attente du fichier
        """
        time.sleep(3)
        try:
            sock_file = socket.socket()
            sock_file.connect((adresse_ip_receveur, int(port)))
        except:
            print(f'tentative de connexion échoué... PORT = {port}')


        f = open(name_file, 'rb')
        l = f.read(1024)
        while(l):
            sock_file.sendall(l)
            l = f.read(1024)
            break
        f.close()

        sock_file.close()


 

          
    def listConnected(self):
        """
            Envoie un requêtes toutes les 2 secondes
            jusqu'à arrêt du serveur
        """
        message = "LISTCONNECTED "
        sock.sendall(message.encode())
        while True:
            time.sleep(2)
            try:
                sock.sendall(message.encode())
            except:
                break

    def clic(self,evt):
        """
            Fonction appelé lorqu'on clic sur un utilisateur dans la liste des users connectés
        """
        index = self.listBox.curselection()
        user = self.userConnect[index[0]]
        self.popUpClient(user)



    def traitementUserList(self, liste):
        """
            Redécoupe la liste reçu pour l'afficher correctement
        """     
        listeSplit = liste.split()
        if len(listeSplit) != 0:
            self.listBox.delete(0, len(self.userConnect))

        self.userConnect = []

        cpt = 1
        for user in listeSplit:
            u = user.split(':')
            if u[0] not in self.userConnect :
                self.userConnect.append(u[0])
            Message = f"{u[0]} : {u[1]}"
            self.listBox.insert(cpt, Message)
            cpt += 1
        self.listBox.bind('<ButtonRelease-1>', self.clic)


    def popUpClient(self, user):
        """
            popUp après clic sur un utilisateur dans la liste
            Choix possible : Chat privée ou Send File
        """

        self.popUpC = Toplevel()

        self.popUpC.geometry("300x150")
        self.popUpC.title(user)

        labelChat = Label(self.popUpC, text="Envoyer une demande de chat",                    
                justify = CENTER,  
                font = "14")
        btnChat = Button(self.popUpC, text="CHAT", command=lambda: self.sendChat(user))
        
        labelFile = Label(self.popUpC, text="Envoyer un fichier",                    
                justify = CENTER,  
                font = "14")
        btnFile = Button(self.popUpC, text="Send File", command=lambda: self.sendFile(user))


        labelChat.pack()
        btnChat.pack(padx=5)
        labelFile.pack()
        btnFile.pack(padx=5)
    
    def popUp_file(self, user_send_file, name_file):
        """
            PupUp pour accepter ou non de recevoir un fichier de pseudo
        """
        self.pop_file = Toplevel()
        self.pop_file.title("Demande recption de fichier")

        self.pop_file.geometry("300x200")

        label_file = Label(self.pop_file, 
                text=f"{user_send_file} souhaite vous envoyer {name_file}",
                justify = CENTER,  
                font = "14")

        label_port = Label(self.pop_file,  
                text = "Choisissez un port", 
                justify = CENTER,  
                font = "14")

        entry_port = Entry(self.pop_file)
        

        btn_accept = Button(self.pop_file, text='Accepter', command=lambda: self.accept_file(user_send_file,name_file, entry_port.get()))
        btn_refuse = Button(self.pop_file, text='Refuser', command=lambda: self.refuse_file(user_send_file, name_file))

        label_file.pack(padx=5)
        label_port.pack(padx=5)
        entry_port.pack(padx=5)
        btn_accept.pack(padx=5)
        btn_refuse.pack(padx=5)
    
    
    def accept_file(self, user_send_file, name_file, port):
        """
            Send ACCEPTFILE with port where listening
        """
        #On recupere l'adresse ip de la machine courante
        if os.name == 'nt':
            _,_,adress = socket.gethostbyname_ex(socket.gethostname())
            ip = adress[-1]
        else:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(("8.8.8.8", 80))
            ip = s.getsockname()[0]
            s.close()

        m = f"{self.name} {user_send_file} {name_file} {port} {ip}"

        snd = threading.Thread(target=self.sendMessage(m, 'ACCEPTFILE '))
        snd.start()

        self.pop_file.destroy()

    def refuse_file(self, user_send_file, name_file):
        """
            Send REFUSEFILE to pseudo
        """
        snd = threading.Thread(target=self.sendMessage(f"{self.name} {user_send_file} {name_file}", 'REFUSEFILE '))
        snd.start()
        self.pop_file.destroy()

    def sendChat(self, user) :
        """
            Envoie une demande de chat si il n'en a pas encore envoyé
        """
        self.popUpC.destroy()

        if user not in self.UserChatPrivee:
            self.UserChatPrivee.append(user)
            snd= threading.Thread(target = self.sendMessage(user, f'CHAT '))         
            snd.start()
            snd.join()

    def sendFile(self, user_receive_file):
        """
            Ouvre une popUp pour envoyé un fichier
        """
        self.popUpC.destroy()

        #filename = filedialog.askopenfilename()
        search_file = filedialog.askopenfilename()
        splt = search_file.split('/', )
        
        name_file = splt[-1]
        user_send_file = self.name
        
        while True:
            if name_file != '':
                msg = f"{user_receive_file} {user_send_file} {name_file}"

                snd = threading.Thread(target=self.sendMessage(msg, "SENDFILE "))
                snd.start()
                break
    


    def fenetreChatPrivee(self, userAmi):
        """
            Creation de la fenetre pour RENAME
        """
        def fermerFenetre():
            #self.fenetreChat.destroy()

            threading.Thread(target=self.sendMessage(userAmi, f'CHAT ')).start()
            #threading.Thread(target=self.sendMessage(self.name, f'CLOSECHAT ')).start()

        self.fenetreChat = Toplevel()
        self.fenetreChat.title(f"Chat privee avec {userAmi}")

        # Impression des messages
        self.TableauMessage = Text(self.fenetreChat, 
                        bg = "#17202A", 
                        fg = "#EAECEE", 
                        font = "14",  
                        pady = 5) 
          

        #Saisie du message
        self.entreeChatPv = Entry(self.fenetreChat)
        self.entreeChatPv.focus()

        # Envoie du message apres appuie sur le button

        buttonQuitter = Button(self.fenetreChat, text='Quitter', command=fermerFenetre) #lambda : threading.Thread(target=self.sendMessage(userAmi, f'CHAT ')).start())
        buttonSend = Button(self.fenetreChat, text="Send", command=lambda : self.sendButton(self.entreeChatPv.get(), f'WHISPER {userAmi.upper()}', self.entreeChatPv))

        self.TableauMessage.pack() 
        self.entreeChatPv.pack(fill=BOTH,expand=1)
        buttonSend.pack()
        buttonQuitter.pack()
        

    def afficherMessage(self, msg):
        """
            Affiche un message sur la fenetre principale
        """

        self.textCons.config(state = NORMAL) 
        self.textCons.insert(END, str(msg)+"\n\n") 
        self.textCons.config(state = DISABLED) 
        self.textCons.see(END)

        



global sock

config = ConfigParser()
config.read('./config_client.ini')

PORT = config['DEFAULT']['port']
HOST = config['DEFAULT']['IP']

try:
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((HOST, int(PORT)))
except:
    print('Connexion impossible...')

 
