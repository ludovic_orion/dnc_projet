#!/usr/bin/python3
import socket 
import sys
import datetime
import threading
import re 
from configparser import ConfigParser

class GroupChat() :
    def __init__(self):
        self.__groupname = ""
        self.__personnes = []

    def __init__(self,name,personnes) :
        self.__groupname = name
        self.__personnes = personnes

    def get_groupname(self):
        return self.__name

    def get_personnes(self):
        return self.__personnes

    
class Client() :
    def __init__(self,socket,pseudo):
        self.__socket = socket
        self.__pseudo = pseudo
        self.__status = 'ACTIVE'
        self.__chats = []
        self.__groupchat = []

    def get_pseudo(self) :
        return self.__pseudo

    def get_socket(self) :
        return self.__socket

    def get_status(self) :
        return self.__status

    def set_pseudo(self,pseudo) :
        self.__pseudo = pseudo

    def set_status(self,status) :
        self.__status = status

    """
    def close_socket(self) :
        self.__socket.close()
    """
    def get_chats(self) :

        return self.__chats

    def get_groupchat(self,groupname) :

        for group in self.__groupchat :
            if group.get_groupname() == groupname :
                return group
        return None

    def check_chat(self,pseudo) :
        """
            Renvoie True si le pseudo est dans le tableau de chat sinon false
        """
        if pseudo in self.__chats :
            return True
        else :
            return False

    def check_group_chat(self,groupname) :
        """ 
            Renvoie True si le nom de groupe est dans le tableau des groupes de chat
        """
        for group in self.__groupchat :
            if group.get_groupname() == groupname :
                return True
        return False

    def manage_group(self,groupname,pseudos) :
        """
            Si le groupe existe deja on supprime le groupe de l'utilisateur
            et on prévient les autres utilisateurs du groupe
            Sinon on va crée le groupe 
            et prévenir les utilisateurs concernés
        """
        reponse = ""
        if self.check_group_chat(groupname) :
            self.__groupchat.remove(self.get_groupchat(groupname))
            reponse = f"161 {groupname} {pseudos}"
            for user in pseudos :
                user = get_user(user)
                user.get_socket().sendall(reponse.encode())
            
        else :
            self.__groupchat.append(GroupChat(groupname, pseudos))
            reponse = f"160 {groupname} {pseudos}"
            for user in pseudos :
                user = get_user(user)
                user.get_socket().sendall(reponse.encode())

        return reponse

    def connect_chat(self,pseudo) :
        """ 
            Connecte deux utilisateurs pour chat (les deux doivent utiliser cette commande)
        """
        username = self.get_pseudo()
        user_2 = get_user(pseudo)
        if pseudo == username :
            return f"360 {pseudo}"
        else :
            if pseudo in self.__chats :

                #Si le pseudo est déjà dans la liste de chat on le supprime
                self.__chats.remove(pseudo)

                #On renvoie un code et pseudo si la commande ferme le chat avec l'autre utilisateur

                if username in user_2.get_chats() :
                    
                    user_2.get_socket().sendall(f"151 {username}".encode())
                    return f"151 {pseudo}" #151   
                else :

                    #Renvoie un code d'erreur si le pseudo n'est pas dans les chats de l'autre
                    return f"301 {pseudo}"
                
            else :
                #Le pseudo n'est pas encore dans la liste de chat alors on l'ajoute
                self.__chats.append(pseudo)

                if username in user_2.__chats:
                    # On previent pseudo que self veut communiquer avec lui
                    
                    user_2.get_socket().sendall(f"152 {username}".encode())
                    return f"150 {pseudo}"
                

                else:
                    # C'est une reponse à une demande alors on connect les deux utilisateurs
                    
                    user_2.get_socket().sendall(f"152 {username}".encode())
                    return f"150 {pseudo}"   

            """
            for chat_name in user_2.get_chats() :

                if chat_name == self.__pseudo :
                    #Code de création de chat avec l'autre utilisateur
                    reponse = f"150 {pseudo}" #150    
                else :
                    #Renvoie un code d'erreur si le pseudo n'est pas dans les chats de l'autre
                    reponse = f"301 {pseudo}"
            """


def check_pseudo(pseudo) :
    """
        Verifie que le pseudo ne soit pas dans la liste de commandes et qu'il corresponde à l'expression regulière
    """

    list_command = {'QUIT','LISTCONNECTED','CONNECT','ACTIVE','AFK','RENAME','CHAT','ALL','WHISPER','SENDFILE','ACCEPT','REFUSEFILE','GROUPCHAT','ADMIN'}
    return (re.search('^[A-Z_0-9]{3,15}$',pseudo) and pseudo not in list_command)

def check_serveur(pseudo) :
    """
        Renvoie True si le pseudo est dans le serveur sinon False
    """

    for client in list_clients :
        if client.get_pseudo() == pseudo :
            return True
    return False

def check_s_p(pseudo) :
    """
        Verifie que le pseudo soit correct et s'il est sur le serveur
    """

    return check_serveur(pseudo) and check_pseudo(pseudo)

def get_user(pseudo) :
    """
        Renvoie le client correspondant au pseudo s'il existe sinon None
    """

    for client in list_clients :
        if client.get_pseudo() == pseudo :
            return client
    return None

def whisper(user,msg) :
    """
        Fonction pour envoyer un message à un autre utilisateur en privé
        Verifie qu'il y a un chat créé entre eux 
    """

    cmdSplit = msg.split(' ',1)
    pseudo = cmdSplit[0]
    user_2 = get_user(pseudo)


    
    #Verifie que les deux personnes soient connectées
    if get_user(pseudo) != None :
        if verif_chat(user,user_2) :
            msg_retour = f"173 {user.get_pseudo()} {cmdSplit[1]}"
            user_2.get_socket().sendall(msg_retour.encode()) #Envoi du message à l'autre utilisateur
        else :
            msg_retour = f"370 {pseudo}"
    else :
        msg_retour = f"302 {pseudo}"

    return msg_retour

def connection_chat(user,pseudo) :
    """
        Fonction pour connecter un utilisateur à un autre à partir de son pseudo
    """
    if check_s_p(pseudo) :
        reponse = user.connect_chat(pseudo)
        return reponse
    else :
        return f"302 {pseudo}"

    #return f"301 {pseudo}"

def verif_chat(user_1,user_2) :

    userpseudo_2 = user_2.get_pseudo()
    userpseudo_1 = user_1.get_pseudo()

    #Verifie que le pseudo de l'autre utilisateur soit dans la variable chat pour chacun
    return user_1.check_chat(userpseudo_2) and user_2.check_chat(userpseudo_1)

def check_status(user) :
    """
        Renvoie True si le status de l'utilisateur est ACTIVE sinon False
    """

    return user.get_status() == "ACTIVE"

def change_status(user,status) :
    """
        Change le status de l'utilisateur par rapport au status donné
    """
    try :
        if user.get_status() == status :
            msg = f"320 {user.get_pseudo()} {status}"
        else :

            user.set_status(status)
            msg = f"120 {user.get_pseudo()} {status}"
            #send_all_msg(user,msg)
            return msg
    except :
        return f"320 {user.get_pseudo()} {status}"

def msg_all(user,message) :
    """
        Commande pour envoyer un message à tout le monde 
    """
    msg_g = f"105 {user.get_pseudo()} {message}"
    msg_err = f"375 {user.get_pseudo()} {message}"
    try : 
        message_all(user.get_pseudo(),msg_g)
        return msg_g
    except :
        return msg_err
        
def message_all(pseudo,message) :
    """
        Envoie un message à tout le monde 
    """
    
    for client in list_clients :
        if client.get_pseudo() != pseudo :
            socket_cl = client.get_socket()
            socket_cl.sendall(message.encode())
    
def list_users(user) :
    """
        Renvoie la liste des utilisateurs avec leur status
    """
    rep = ""
    for client in list_clients :
        rep+= f" {client.get_pseudo()}:{client.get_status()}"
    return f"100{rep}"

def send_to(pseudo,msg) :
    """
        Envoie un message specifique à une personne (utilisé par le serveur et non les clients)
    """
    client = get_user(pseudo)
    socket_client = client.get_socket()
    socket_client.sendall(msg.encode())

def quit(user_client) :
    """
        Commande pour se deconnecter et prevenir tout le monde de la deconnexion
    """
    
    try :
        pseudo = user_client.get_pseudo()
        reponse = f"190 {pseudo}"
        
        try :
            message_to_all = f"195 {pseudo}"
            message_all(pseudo,message_to_all)
            list_clients.remove(user_client)
        except :
            reponse = f"300 {pseudo}"
    except :
        reponse = f"300"
    return reponse

def rename_user(user,pseudo) :
    """
        Commande pour se renommer qui verifie que le pseudo ne soit pas sur le serveur 
        et qu'il corresponde bien aux critères de nommage de pseudo

    """

    if check_pseudo(pseudo):

        if not check_serveur(pseudo) :

            ancien_pseudo = user.get_pseudo()
            user.set_pseudo(pseudo)
            rep_all = f"142 {ancien_pseudo} {pseudo}"
            message_all(pseudo,rep_all)

            rep = f"141 {ancien_pseudo} {pseudo}"
            message_all(user,rep)
            return f"142 {ancien_pseudo} {pseudo}"

        else :
            rep = f"340 {pseudo}"
            return rep
    else :
        rep = f"310 {pseudo}"
        return rep
    message_all(user,rep)


def request_file(user,command) :
    """
        Envoie une demande à un utilisateur avec le nom du fichier qu'il veut envoyer
    """
    cmdSplit = command.split()
    user_receive_file = cmdSplit[0]
    user_send_file = cmdSplit[1]
    name_file = cmdSplit[2]

    message_for_pseudo = f"192 {user_receive_file} {user_send_file} {name_file}" #Verif code erreur


    if check_s_p(user_receive_file):
        try:
            send_to(user_receive_file,message_for_pseudo)
            reponse = "191" #Verif code erreur
        except:
            reponse = "392" #Verif code erreur
    else :
        reponse = f"399 {user_receive_file}"

    return reponse


def accept_file(user,command) :
    """
        Envoie une reponse positive à l'utilisateur en lui fournissant le port et le protocole et le nom du fichier
    """
    reponse = "" 
    cmdSplit = command.split()
    user_receive_file = cmdSplit[0]
    user_send_file = cmdSplit[1]
    name_file = cmdSplit[2]
    port = cmdSplit[3]
    adresse_ip = cmdSplit[4]

    message_for_pseudo = f"193 {user_receive_file} {name_file} {port} {adresse_ip}" #Verif code erreur

    if check_s_p(user_send_file):
        try:
            send_to(user_send_file,message_for_pseudo)
            reponse = f"196 {name_file} {port}" #Verif code erreur
        except :
            reponse = "397" #Verif code erreur ,398
    return reponse



def refuse_file(user,command) :
    """ 
        Envoie une réponse négative à l'utilisateur avec le nom du fichier qu'il n'accepte pas
    """
    reponse = "" 
    cmdSplit = command.split()
    user_receive_file = cmdSplit[0]
    user_send_file = cmdSplit[1]
    name_file = cmdSplit[2]
    message_for_pseudo = f"194 {user_receive_file} {name_file}" #Verif code erreur

    if check_s_p(user_send_file):
        try:
            send_to(user_send_file,message_for_pseudo)
            reponse = "191 " #Verif code erreur
        except :
            reponse = "392" #Verif code erreur
    return reponse

def group_chat(user,command) :
    """ 
        Creation d'un groupe de chat si le nom du groupe est correct 
    """
    cmdSplit = command.split(' ',1)
    groupname = cmdSplit[0]
    tableau_pseudos = cmdSplit[1].split(' ')
    if check_pseudo(groupname) :
        rep = user.manage_group(groupname, tableau_pseudos)
        for pseudo in tableau_pseudos :
            
            send_to(pseudo, rep)
            return rep
        
    else :
        return f"361 {groupname}"

def GestionRequete(client):
    """
        Gestion de la commande
        msg est tableau contenant le contenu de la commande
    """
    pseudo = ""
    connected = False
    while not connected: #Verifier que le pseudo est correct ici

        requete = client.recv(TAILLE_TAMPON)
        reponse = requete.decode()
        msgSplit = reponse.split(' ',1)
        if msgSplit[0].upper() == 'CONNECT' : #Verifier que le pseudo est correct ici
            pseudo = msgSplit[1].upper()
            if check_pseudo(pseudo):
                if not check_serveur(pseudo) :
                    user_client = Client(client,pseudo)
                    list_clients.append(user_client)

                    msgBienvenue = f"110 {user_client.get_pseudo()}"
                    message_all(user_client.get_pseudo(),msgBienvenue)
                    user_client.get_socket().sendall(msgBienvenue.encode())
                    connected = True
                else :
                    client.sendall(f"340 {pseudo}".encode())
                
            else :
                client.sendall(f"310 {pseudo}".encode())
        else :
            client.sendall(f"400".encode())

    while True:
        try:
            reponse = ""
            requete = client.recv(TAILLE_TAMPON)
            dateTime = datetime.datetime.now()  
            time = dateTime.strftime('%X') 

            mess = requete.decode()
            #ip_client, port_client = adr_client

            #print(f"Requete provenant de {ip_client}:{port_client}. Longueur = {len(mess)}")

            
            msgSplit = mess.split(' ', 1)
            command = msgSplit[0].upper()
            if not check_status(user_client) :

                if command == '' or command == 'QUIT': 
                    reponse = quit(user_client)
                    client.sendall(reponse.encode())
                    break
                elif command == "ACTIVE" :
                    reponse = change_status(user_client,'ACTIVE')
                else :
                    reponse = "400"
            elif check_status(user_client) :
                if command == '' or command == 'QUIT': 
                    reponse = quit(user_client)
                    client.sendall(reponse.encode())
                    break

                elif command == 'LISTCONNECTED' :
                    reponse = list_users(user_client)

                elif command == 'AFK' :
                    reponse = change_status(user_client,'AFK')
                    
                elif command == 'RENAME' :
                    reponse = rename_user(user_client,msgSplit[1])

                elif command == 'CHAT' :
                    reponse = connection_chat(user_client,msgSplit[1])

                elif command == 'ALL' :
                    reponse = msg_all(user_client,msgSplit[1])
                    
                
                elif command == 'WHISPER' :
                    reponse = whisper(user_client,msgSplit[1])
            
                elif command == 'SENDFILE':
                    reponse = request_file(user_client,msgSplit[1])

                elif command == 'ACCEPTFILE' :
                    reponse = accept_file(user_client,msgSplit[1])

                elif command == 'REFUSEFILE' :
                    reponse = refuse_file(user_client,msgSplit[1])

                elif command == 'GROUPCHAT' :
                    reponse = group_chat(user_client,msgSplit[1],msgSplit[2])

                else :
                    reponse = "400"


            client.sendall(reponse.encode())

            with open('serveur_log.log', "a") as f:
                
                    
                sys.stdout = f
                sys.stdout.write(f"[{time}] - client:{user_client.get_pseudo()} : reponse : {reponse} \n")

            #envoie de la reponse
            

            
        except : 
            try :
                quit(user_client)
                break
            except :
                break
    client.close()

        

if __name__ == "__main__" :

    config = ConfigParser()
    config.read('./config.ini')
    
    port = config['DEFAULT']['port']
    log_file = config['DEFAULT']['log_file']
    path_file = config['DEFAULT']['path_access']

    TAILLE_TAMPON = 256
    PORT = int(port)

    sock_server = socket.socket()
    sock_server.bind(('', PORT))
    sock_server.listen(4)


    with open(log_file, "a") as f:
        sys.stdout = f
        msgDebut = f"{datetime.datetime.now().strftime('%x %X')}  Serveur en attente sur le port {port} log stockés dans {log_file}\n"
        sys.stdout.write(msgDebut)
            
    print(msgDebut, file=sys.stderr)

    
    list_clients = []

    while True:
        try:
            sock_client, adr_client = sock_server.accept()
            threading.Thread(target=GestionRequete, args=(sock_client,)).start()
        except KeyboardInterrupt:
            break
    
    for t in threading.enumerate():
        if t!= threading.main_thread() : t.join()
    
    sock_server.close() #fermeture de la socket serveur
    print("Arrêt du serveur", file=sys.stderr)

    
    sys.exit(0)
