Jean Jaures Toulouse                                            L. ORION
Request for Comments: 9098                                    A. DJABIRI
                                                           R. THEVENIAUT
                                                             C. OUHAMMOU
                                                              March 2021


                    Dog is Not a Chat - Version 1

Status of this Memo

   This document specifies an Internet standards DNC protocol for the 
   Internet Community, which enable client to connect it to a DNC 
   server, and enable DNC server to chat with all clients via TCP connection.

Table of Contents

   1. Introduction ................................................    1
   2. Send us a message ...........................................    1
   3. Basic Operation .............................................    2
   4. The DISCONNECTED State.......................................    2
      CONNECT Command .............................................    2
   5. The ACTIVE State.............................................    3
      QUIT Command ................................................    3
      AFK Command .................................................    3
      LISTCONNECTED Command .......................................    4
      RENAME Command ..............................................    4
      CHAT Command ................................................    5
      ALL Command .................................................    5
      WHISPER Command .............................................    6
      SENDFILE Command ............................................    6
      ACCEPTFILE Command ..........................................    7
      REFUSEFILE Command ..........................................    7
   6. The AFK State................................................    8 
      ACTIVE Command ..............................................    8
   7.DNC Optional Commands.........................................    8
     GROUPCHAT.....................................................    8
   8.DNC Summary...................................................    9
     Minimal DNC Commands..........................................    9
     Optionnal DNC Commands........................................    9
     DNC Replies...................................................    9
   9.Acknowledgements.............................................    10
   10.Authors' Addresses...........................................    11
  
Purple Team                Standards Track                     [Page 1]

RFC 9098                          DNC                          March 2021


1. Introduction

	 DNC server awaits client connexions which MUST propose a pseudo. 
   If the pseudo is not  already  assigned, the server connects the  client  with 
   asked pseudo. Surnames are not saved. id est, if the client disconnects
   from DNC  Server, he  loses his pseudo. The  pseudo can  be assigned.
   Another  client  can take it. When the  client  is  connected, all other 
   clients are warned.

2. Send us a message

   This memo explains what is a message and what it contains.
   	
    The message  is sent  to the Server.  The  server then shares the 
    message to all other clients. The message starts by the surname of the 
    client who  sent the	message  or by the  name of the server (if it's
    a status message).















Purple Team                Standards Track                     [Page 2]

RFC 9098                          DNC                          March 2021

3. Basic Operation
	
   Initially, the server host starts the DNC service by listening on the 
   TCP port 8000 by default, it CAN be changed in a config file. Some 
   client can  connect to the server. When a client wishes to make use
   of the  service, he  must choose  a surname. If this surname	is not
   already  taken, the connection  is established with the server host.
   When the  connection  is  established, the  server send a  welcoming
   message and alert other connected users. When a client disconnects,
   the server MUST alert other connected users and the connection will be
   closed.
   
   Connected  clients can send a  message  to the server which  then send it 
   to all users. Otherwise, a  client can send a private  message or a file 
   to another one who can decide to accept or decline the dialogue. The message 
   will be preceded by the  user's nickname  and a file will be preceded by 
   the word "file :". If a  user have a  private  dialogue with another one, 
   another user can still send him a private message.
   
   Commands will be in camel Case and may be up to 5 words, possibly followed 
   by one or more arguments
   
   Responses  to  certain  commands are  multi-line, for  example  message 
   returned by the server or messages sent between users. 
   
   The server must respond to an unrecognized, unimpliment, or syntactically 
   invalid command to return the type of error.
   
   A client may have an inactivity autologout timer.  Such a timer
   must be of at least of 30 minutes duration. Receiving any command
   from the client during that interval should be enough to reset the
   autologout timer.  When the timer expires, the client is disconnected.
   The server should close the TCP connection with this client and send
   a disconnection message to everyone.

4. The DISCONNECTED State

   In this state the client needs to identify himself to the DNC server,
   if the connection is successfull he then enters the ACTIVE state.

	    CONNECT username

         Arguments: 
         		 A username (required) to connect to the server. 

         Restrictions: 
         		  The username MUST NOT exist on the list of connected
              users on the server. 

         Discussion:
             If the username does NOT exist on the list of connected
             users, the DNC server issues a positive response with a line
             containing information about the Client STATUS and a 
             welcoming message. The server use the command WELCOME


         Possible Responses:
             110
             310
             311

         Examples:
             C: CONNECT Michel
             S: 110
             ...
             C: CONNECT Michel
             S: 311


Purple Team                Standards Track                     [Page 3]

RFC 9098                          DNC                          March 2021


5. The ACTIVE State

   Once the client has successfully identified itself to the DNC server
   the DNC session changes to the ACTIVE state.  The client may now
   issue any of the following DNC commands repeatedly.  After each
   command, the DNC server issues a response.  Eventually, the client
   issues the QUIT command and the DNC session enters the (UPDATE) state.

   Here are the DNC commands valid in the ACTIVE state:
   
		 QUIT

         Arguments: 
         		 none. 

         Restrictions: 
         		none. 

         Discussion:
             If the username does NOT exist on the list of connected
             users, the DNC server issues an error. If the username exists
             on the server it will disconnect him from it and send a message
             to all the connected users left.
             


         Possible Responses:
             190
             195
             300

         Examples:
             C: QUIT
             S: 190
             ...
             C: QUIT
             S: 300

         AFK

         Arguments: none

         Restrictions: none

         Discussion:
             The DNC server issues a positive response with a line
             containing information about the Client STATUS.


         Possible Responses:
             120 
             320 

         Examples:
             C: AFK
             S: 120
             ...
             C: AFK
             S: 320


 			
Purple Team                Standards Track                     [Page 4]

RFC 9098                          DNC                          March 2021
             
      LISTCONNECTED

         Arguments: none
             
 
         Restrictions:
             Client STATUS MUST be ACTIVE

         Discussion:
             If the DNC server issues a positive response, then the
             response given is multi-line.  After the initial 100, the
             DNC server sends the list of connected users with their 
             name and their status.
             (Format : "100: Name1 : STATUS; Name2 : STATUS;...")
             
         Possible Responses:
             100

         Examples:
             C: LISTCONNECTED
             S: 100: Name1 : STATUS; Name2 : STATUS
             
      RENAME newSurname
      
         Arguments:
             a newSurname (required) which may NOT refer to a
             surname already existing on the server.

         Restrictions:
             Client STATUS MUST be ACTIVE
             symbols authorized : [a-zA-Z0-9_-]{3,15} 

         Discussion:
             The DNC server changes the name given by the client
             to this new name. Then, it returns a welcome message
             with the new name

         Possible Responses:
             142
             141
             310
             340 

         Examples:
             C: RENAME Bernard
             S: 140
                ...
             C: RENAME Bernard
             S: 340
                ...
             C: RENAME Bern@rd
             S: 310

Purple Team                Standards Track                     [Page 5]

RFC 9098                          DNC                          March 2021

      CHAT userName
         Arguments: 
		A userName (required) which may NOT refer to the client
             surname, userName MUST exists in the server.

         Restrictions:
             none.

         Discussion:
             The DNC server receives the request, it sends a request to
             the other user and ask him to confirm the connection.
             If he accepts, the server will connect both users to 
             a unique chat, only visible for them. If a user wants to 
             stop a chat he can use the same request.

	     Possible Responses:
             150
             151
             152
             301
             381
             
         Examples:
             C: CHAT Bernard
             S: 150
             ...
             C: CHAT Bernard
             S: 151
             C: CHAT Bernar
             S: 300

		ALL message

         Arguments: 
         A message(required) that will be sent to everyone.
         

         Restrictions:
             none.

         Discussion:
             The DNC server checks if the user is connected, then 
             send the message to every other users.
             
         Possible Responses:
             105
             375
             	 
         Examples:
         		 
             C: ALL "Hello everyone"
             S: 105


Purple Team                Standards Track                     [Page 6]

RFC 9098                          DNC                          March 2021

  		WHISPER username message

         Arguments: 
         	A username (required) and a message(required),userName 
             MUST exists in the server.
         

         Restrictions:
             Client MUST be connected in a chat with Username
             to be able to WHISPER him.

         Discussion:
             The DNC server checks if both clients are connected
             together. If so, the message is sent to the other client
             with a positive response.

         Possible Responses:
             173
             301
             370

         Examples:
         		 
             C: WHISPER Michel coucou
             S: 170
             C: WHISPER Manon hello
             S: 370


  SENDFILE userName file

         Arguments: 
		A userName (required) which may NOT refer to the client
             surname, userName MUST exists in the server.
             

         Restrictions:
             none.

         Discussion:
             The DNC server receive the request and send a request to
             the other user and ask him to accept the sending of the file.
             The file will be send between both users and not through the
             server.


         Possible Responses:
             192
             191
             391
             392
             399
             

         Examples:
             C: SENDFILE Bernard coucou.txt
             S: 192
             ...
             C: SENDFILE Bernard bipbip.exe
             S: 391  

Purple Team                Standards Track                     [Page 7]

RFC 9098                          DNC                          March 2021

        ACCEPTFILE Username [port] [protocol]

         Arguments:
	 	A command (required) and a username (required)
		A port and a protocol when user want to accept a
             sendfile request.
             

         Restrictions:
             Command and username MUST exist in the server.
             Port MUST correspond to the right protocol. 

         Discussion:
             The user give a command and a username to the DNC
             server with optionnally port and protocol for a SENDFILE.
             The DNC server will then send back to username the arguments
             he needs to confirm his requests.
             


         Possible Responses:
             193
             196
             397
               

		REFUSEFILE userName file

         Arguments: 
	     A userName (required) which may NOT refer to the client
             surname, userName MUST exists in the server.
             

         Restrictions:
             none.

         Discussion:
             The DNC server receive the request and send a request to
             the other user and ask him to accept the sending of the file.
             The file will be send between both users and not through the
             server.


         Possible Responses:
             194
             191
             392
             
Purple Team                Standards Track                    [Page 8]

RFC 9098                          DNC                          March 2021

6. The AFK State

   When the client issues the AFK command, his state changes to AFK,
   he cannot send messages anymore but he can still receive them. His
   inactivity timer will be set to 1 hour and 30 minutes instead of 30 
   minutes. The only command the client will be able to use is ACTIVE.

      ACTIVE

         Arguments: none
         
				 Restrictions: none

         Discussion:
             The DNC server issues a positive response with a line 
             containing information about the Client STATUS
             

         Possible Responses:
             120
             320

         Examples:
             C: ACTIVE
             S: 120
             ...
             C: ACTIVE
             S: 320

      
7. DNC Optional commands

		  GROUPCHAT groupname name1 name2 ...

         Arguments:
             a groupname (required) which MUST not be a username existing.
             at least two other different users who MUST be on the server.

         Restrictions: All users requested MUST be in ACTIVE state.
             

         Discussion:
             A user send a request to the server asking to create a chatgroup.
             The DNC server send then a request to every other users given in 
             the arguments. If they all accept, the server will create a chat 
             between these multiple users.

       Possible Responses:
             160
             360
             310
             381

         Examples:
             C: GROUPCHAT team1 Michel Bernard
             S: Asking other users...
             S to Michel/Bernard : Accept groupchat with C ? [y/n]
             S to C/Michel/Bernard : Group created with name : team1

Purple Team                Standards Track                     [Page 9]

RFC 9098                          DNC                          March 2021

8. DNC Command Summary

      Minimal DNC Commands:
      	 CONNECT name			 valid in the DISCONNECTED state.

         ACTIVE            	     valid in the AFK state.
         
         QUIT                    valid in the ACTIVE state.
         AFK
         LISTCONNECTED
         RENAME name
         CHAT name
         ALL msg
         WHISPER name msg
         SENDFILE name file
         ACCEPTFILE name port protocol
         REFUSEFILE name file
         

      Optional DNC Commands:

         GROUPCHAT chatname name1 name2 ...        valid in the ACTIVE state

      DNC Replies:

         1XX
         3XX

      The reply given by the DNC server to any command is significant
      only to "1XX" and "3XX".  Any text occurring after this reply
      may be ignored by the client.

Purple Team                Standards Track                    [Page 10]

RFC 9098                          DNC                          March 2021

9. Acknowledgements

	 Here is the list of all the issues the server can send back.

   +------+---------------------------------------------------------+
   | Code | Reason-Phrase                                           |
   +------+---------------------------------------------------------+
   | 100  | list                                                    |
   | 105  | Username sent message                                   |
   | 110  | Welcome on the server Username                          |
   | 120  | User status                                             |
   | 141  | ClientName to newSurname to other                       |
   | 142  | ClientName to newSurname to self user                   |
   | 150  | Chat request UserName                                   |
   | 151  | Chat close UserName                                     |
   | 152  | Chat created with user1 and user2                       |
   | 160  | groupName create                                        |
   | 173  | Message sent to UserName                                |
   | 175  | Message sent to everyone                                |
   | 180  | File sent to userName                                   |
   | 190  | UserName disconnects                                    |
   | 191  | Request sent                                            | 
   | 192  | Request to send file                                    |
   | 193  | Request accepted                                        |
   | 194  | Request refused                                         |
   | 196  | Accept request sent to user                             | 
   | 195  | Wrong Name                                              |
   |      | -------------------   Errors   ------------------------ |
   | 301  | no userName on server                                   |
   | 310  | Wrong typing of name.                                   |
   | 311  | username is already connected on the server             |
   | 320  | User already in current state                           |
   | 340  | userName on server                                      | 
   | 341  | bad newSurname                                          |
   | 360  | invalid username                                        |
   | 361  | invalid groupname                                       |
   | 363  | Request refused by name1                                |
   | 370  | no chat username                                        |
   | 375  | cannot send message                                     |
   | 381  | Request refused by username                             |
   | 397  | Wrong protocol or port                                  |
   | 400  | Invalid command                                         |
   +------+---------------------------------------------------------+


Purple Team                Standards Track                    [Page 11]

RFC 9098                          DNC                          March 2021


10. Authors' Addresses

   Ludovic Orion
   119 impasse de Roquemaurel
   31300 Toulouse
   France

   EMail: ludovic.orion@outlook.fr

	 Cyphax Ouhammou
   30 Rue Palaprat
   31000 Toulouse
   France
   
   EMail: cyphax.ouhammou@gmail.com

	 Robin Theveniaut
   32bis rue des fontaines
   31300 Toulouse
   France
   
   Email: robin.theveniaut@club-internet.fr
   
   Ambasse Djabiri
   77 Rue saint jean 
   31130 Balma
   France
   
   Email: ambass.d@outlook.com
